#!/usr/bin/env python
# -*- coding: utf-8 -*-
# pylint: disable=W0613, C0116
# type: ignore[union-attr]
# This program is released under AGPL-v3
# This program is based on code dedicated to the public domain
# under the CC0 license.  https://git.io/JIaNP

"""
Simple Bot to reply to Telegram messages.

First, a few handler functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Sends Hijri date and Ramadan month time. Use /hijri to get Hjiri date
Use /ramadan to get Ramdan date and days remaning
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

import logging
import convert_numbers

from datetime import datetime, timedelta

from hijri_converter import convert

from telegram import ParseMode, Update
from telegram.ext import Updater, CommandHandler, MessageHandler,Filters, CallbackContext

#Storing the help message outside
help_message = "Hi! how can I help you"

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)

logger = logging.getLogger(__name__)


# Calculate if Ramadan month have passed or not, return the year next
# Ramdan is in.

def get_ramadan_year():
    global ramadan_year
    
    today_hijri = convert.Gregorian.today().to_hijri()
    hijri_tuple = today_hijri.datetuple()
    
    if hijri_tuple[1] >= 8:
        ramadan_year = today_hijri.year + 1
    else:
        ramadan_year = today_hijri.year
    return ramadan_year
get_ramadan_year()

# Define a few command handlers. These usually take the two arguments update and
# context. Error handlers also receive the raised TelegramError object in error.

def start(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /start is issued."""
    update.message.reply_text('Hi!')

def help_command(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /help is issued."""
    update.message.reply_text(help_message)

# Prints Hjiri date when requested using /hijri, using convert_numbers
# library to show Arabic numbers.

def hijri(update, context):
    """Send today Hijri date"""
    hijri = convert.Gregorian.today().to_hijri()
    hijri_format_raw = (hijri.day_name("ar"),
                        convert_numbers.english_to_arabic(hijri.day),
                        hijri.month_name("ar"),
                        convert_numbers.english_to_arabic(hijri.year)
                        )
    # using join on the tuble to create one object and converting it to string
    hijri_format = str(" ".join(map(str, hijri_format_raw)))
    update.message.reply_text(hijri_format)


def ramadan(update, context: CallbackContext) -> None:
    """Send ramdan date"""
    gregorian_ramadan = convert.Hijri(ramadan_year, 9, 1).to_gregorian()
    ramadan_start_after = gregorian_ramadan - datetime.now().date()
    update.message.reply_text( "First day of Ramadan " + str(gregorian_ramadan) + "\n" + "Ramadan start after " +str(ramadan_start_after.days) + " days" + "\n" + "cool down!")
    #update.message.reply_text(ramazon_date)

## commenting the echo function
#def echo(update: Update, context: CallbackContext) -> None:
#    """Echo the user message."""
#    update.message.reply_text(update.message.text)

    # replay to unknown commands with this message
def unknown(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, text="Sorry, I didn't understand that command.")


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def main():
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater("TOKEN", use_context=True)

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # on different commands - answer in Telegram
    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("help", help_command))
    dispatcher.add_handler(CommandHandler("hijri", hijri))
    dispatcher.add_handler(CommandHandler("ramadan", ramadan))
    ##commenting the echo
    ## on noncommand i.e message - echo the message on Telegram
    #dispatcher.add_handler(MessageHandler(Filters.text & ~Filters.command, echo))

    # handle unkown commands
    unknown_handler = MessageHandler(Filters.command, unknown)
    dispatcher.add_handler(unknown_handler)
    # log all errors
    dispatcher.add_error_handler(error)

    # Start the Bot
    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
